var pages = document.getElementsByClassName("page");

window.onload = function(){
    start();
}

    function start(){
        console.log(pages);
        pages[0].onclick = ani;
        pages[1].onclick = aniOne;
        pages[2].onclick = aniTwo;
        pages[3].onclick = aniThree;
        pages[4].onclick = aniFour;
        pages[5].onclick = aniFive;
        pages[6].onclick = aniSix;
        pages[7].onclick = aniSeven;
        pages[8].onclick = aniEight;
        pages[9].onclick = aniNine;
        pages[10].onclick = aniTen;
        pages[11].onclick = aniEleven;
    }

    function ani(){
        var pageOne = document.getElementById("page0");
        pageOne.classList.add("pageAnimation");
        pageOne.addEventListener("animationend", myEndFunction);
    }

    function myEndFunction(){
        var page = document.getElementById("page1");
        page.style.zIndex = 0;
    }

    function aniOne(){
        var pageOne = document.getElementById("page1");
        pageOne.classList.add("pageAnimation");
        pageOne.addEventListener("animationend", endOne);
    }

    function endOne(){
        var page = document.getElementById("page2");
        page.style.zIndex = 0;
    }

    function aniTwo(){
        var pageOne = document.getElementById("page2");
        pageOne.classList.add("pageAnimation");
        pageOne.addEventListener("animationend", endTwo);
    }

    function endTwo(){
        var page = document.getElementById("page3");
        page.style.zIndex = 0;
    }

    function aniThree(){
        var pageOne = document.getElementById("page3");
        pageOne.classList.add("pageAnimation");
        pageOne.addEventListener("animationend", endThree);
    }

    function endThree(){
        var page = document.getElementById("page4");
        page.style.zIndex = 0;
    }

    function aniFour(){
        var pageOne = document.getElementById("page4");
        pageOne.classList.add("pageAnimation");
        pageOne.addEventListener("animationend", endFour);
    }

    function endFour(){
        var page = document.getElementById("page5");
        page.style.zIndex = 0;
    }

    function aniFive(){
        var pageOne = document.getElementById("page5");
        pageOne.classList.add("pageAnimation");
        pageOne.addEventListener("animationend", endFive);
    }

    function endFive(){
        var page = document.getElementById("page6");
        page.style.zIndex = 0;
    }

    function aniSix(){
        var pageOne = document.getElementById("page6");
        pageOne.classList.add("pageAnimation");
        pageOne.addEventListener("animationend", endSix);
    }

    function aniSeven(){
        var pageOne = document.getElementById("page7");
        pageOne.classList.add("pageAnimation");
        pageOne.addEventListener("animationend", endSeven);
    }

    function aniEight(){
        var pageOne = document.getElementById("page8");
        pageOne.classList.add("pageAnimation");
        pageOne.addEventListener("animationend", endEight);
    }

    function aniNine(){
        var pageOne = document.getElementById("page9");
        pageOne.classList.add("pageAnimation");
        pageOne.addEventListener("animationend", endNine);
    }

    function aniTen(){
        var pageOne = document.getElementById("page10");
        pageOne.classList.add("pageAnimation");
        pageOne.addEventListener("animationend", endTen);
    }

    function aniEleven(){
        var pageOne = document.getElementById("page11");
        pageOne.classList.add("pageAnimation");
        pageOne.addEventListener("animationend", endEleven);
    }

function endSix(){
    var page = document.getElementById("page7");
    page.style.zIndex = 0;
}
function endSeven(){
    var page = document.getElementById("page8");
    page.style.zIndex = 0;
}
function endEight(){
    var page = document.getElementById("page9");
    page.style.zIndex = 0;
}
function endNine(){
    var page = document.getElementById("page10");
    page.style.zIndex = 0;
}
function endTen(){
    var page = document.getElementById("page11");
    page.style.zIndex = 0;
}
function endFEleven(){
    var page = document.getElementById("page12");
    page.style.zIndex = 0;
}

