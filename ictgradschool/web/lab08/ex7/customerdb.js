window.onload = function(){
	var table = document.getElementById("table");
	for (var i = 0; i < customers.length; i++) {
		var row = document.createElement("tr");
		table.appendChild(row);

		//add name, gender, year, joined and num to table
		var data = document.createElement("td");
		row.appendChild(data);
		data.innerHTML = customers[i].name;

		var data1 = document.createElement("td");
		row.appendChild(data1);
		data1.innerHTML = customers[i].gender;

		var data2 = document.createElement("td");
		row.appendChild(data2);
		data2.innerHTML = customers[i].year_born;

		var data3 = document.createElement("td");
		row.appendChild(data3);
		data3.innerHTML = customers[i].joined;

		var data4 = document.createElement("td");
		row.appendChild(data4);
		data4.innerHTML = customers[i].num_hires;
	}

	var maleNumber = 0;
	var femaleNumber = 0;
	for(var i = 0; i < customers.length; i++){
		var gender = customers[i].gender;
		if(gender == "male"){
			maleNumber++;
		}
		else{
			femaleNumber++;
		}
	}

	var stats = document.getElementById("stats");

	//create heading for summary stats
    var heading = document.createElement("h3");
	stats.appendChild(heading);
	heading.innerHTML = "Summary Statistics"

	//add gender stats
	var female = document.createElement("p");
    stats.appendChild(female);
    female.innerHTML = "The number of females is " + femaleNumber;

    var male = document.createElement("p");
    stats.appendChild(male);
    male.innerHTML = "The number of males is " + maleNumber;

    var youngNumber = 0;
    var mediumNumber = 0;
    var agedNumber = 0;

    for(var i = 0; i < customers.length; i++){
        var age = 2017 - customers[i].year_born;
        if(age >= 0 && age <= 30){
            youngNumber++;
        }
        else if (age >= 31 && age <= 64){
            mediumNumber++;
        }
        else{
            agedNumber++;
        }
    }

    //add age stats
    var young = document.createElement("p");
    stats.appendChild(young);
    young.innerHTML = "The number of younguns is " + youngNumber;

    var medium = document.createElement("p");
    stats.appendChild(medium);
    medium.innerHTML = "The number of people in mid-life crisis is " + mediumNumber;

    var aged = document.createElement("p");
    stats.appendChild(aged);
    aged.innerHTML = "The number of the aged and wise is " + agedNumber;

    //found out how many videos they hired per week using joined year, 2017 and num_hires
    goldNumber = 0;
    silverNumber = 0;
    bronzeNumber = 0;

    for(var i = 0; i < customers.length; i++){
        var joined = customers[i].joined;
        var weeks = (2017 - joined) * 52;
        var weekly = customers[i].num_hires/weeks;
        if(weekly > 4){
            goldNumber++;
        }
        else if (weekly >= 1 && weekly <= 4){
            silverNumber++;
        }
        else{
            bronzeNumber++;
        }
    }

    //add loyalty status stats
    var gold = document.createElement("p");
    stats.appendChild(gold);
    gold.innerHTML = "The number of customers who shine like gold is " + goldNumber;

    var silver = document.createElement("p");
    stats.appendChild(silver);
    silver.innerHTML = "The number of customers who visit us somewhat frequently is " + silverNumber;

    var bronze = document.createElement("p");
    stats.appendChild(bronze);
    bronze.innerHTML = "The number of customers who don't really like visiting us is " + bronzeNumber;

}

var customers = [
	{ "name" : "Peter Jackson", "gender" : "male", "year_born" : 1961, "joined" : "1997", "num_hires" : 17000 },
		
	{ "name" : "Jane Campion", "gender" : "female", "year_born" : 1954, "joined" : "1980", "num_hires" : 30000 },
	
	{ "name" : "Roger Donaldson", "gender" : "male", "year_born" : 1945, "joined" : "1980", "num_hires" : 12000 },
	
	{ "name" : "Temuera Morrison", "gender" : "male", "year_born" : 1960, "joined" : "1995", "num_hires" : 15500 },
	
	{ "name" : "Russell Crowe", "gender" : "male", "year_born" : 1964, "joined" : "1990", "num_hires" : 10000 },
	
	{ "name" : "Lucy Lawless", "gender" : "female", "year_born" : 1968, "joined" : "1995", "num_hires" : 5000 },	
		
	{ "name" : "Michael Hurst", "gender" : "male", "year_born" : 1957, "joined" : "2000", "num_hires" : 15000 },
		
	{ "name" : "Andrew Niccol", "gender" : "male", "year_born" : 1964, "joined" : "1997", "num_hires" : 3500 },	
	
	{ "name" : "Kiri Te Kanawa", "gender" : "female", "year_born" : 1944, "joined" : "1997", "num_hires" : 500 },	
	
	{ "name" : "Lorde", "gender" : "female", "year_born" : 1996, "joined" : "2010", "num_hires" : 1000 },	
	
	{ "name" : "Scribe", "gender" : "male", "year_born" : 1979, "joined" : "2000", "num_hires" : 5000 },

	{ "name" : "Kimbra", "gender" : "female", "year_born" : 1990, "joined" : "2005", "num_hires" : 7000 },
	
	{ "name" : "Neil Finn", "gender" : "male", "year_born" : 1958, "joined" : "1985", "num_hires" : 6000 },	
	
	{ "name" : "Anika Moa", "gender" : "female", "year_born" : 1980, "joined" : "2000", "num_hires" : 700 },
	
	{ "name" : "Bic Runga", "gender" : "female", "year_born" : 1976, "joined" : "1995", "num_hires" : 5000 },
	
	{ "name" : "Ernest Rutherford", "gender" : "male", "year_born" : 1871, "joined" : "1930", "num_hires" : 4200 },
	
	{ "name" : "Kate Sheppard", "gender" : "female", "year_born" : 1847, "joined" : "1930", "num_hires" : 1000 },
	
	{ "name" : "Apirana Turupa Ngata", "gender" : "male", "year_born" : 1874, "joined" : "1920", "num_hires" : 3500 },
	
	{ "name" : "Edmund Hillary", "gender" : "male", "year_born" : 1919, "joined" : "1955", "num_hires" : 10000 },
	
	{ "name" : "Katherine Mansfield", "gender" : "female", "year_born" : 1888, "joined" : "1920", "num_hires" : 2000 },
	
	{ "name" : "Margaret Mahy", "gender" : "female", "year_born" : 1936, "joined" : "1985", "num_hires" : 5000 },
	
	{ "name" : "John Key", "gender" : "male", "year_born" : 1961, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Sonny Bill Williams", "gender" : "male", "year_born" : 1985, "joined" : "1995", "num_hires" : 15000 },
	
	{ "name" : "Dan Carter", "gender" : "male", "year_born" : 1982, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Bernice Mene", "gender" : "female", "year_born" : 1975, "joined" : "1990", "num_hires" : 30000 }	
];